import React from 'react'
import Books from './components/Books/Books';
import Login from './components/Login/Login';
import Register from './components/Register/Register';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Book from './components/Books/Book';
import Settings from './components/Settings/Settings'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Login/>} />
        <Route path='/register' element={<Register/>} />
        <Route exact path='/books' element={<Books/>} />
        <Route path='/books/:id' element={<Book/>}/>
        <Route path='/settings' element={<Settings/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
