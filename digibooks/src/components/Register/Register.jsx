import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import credentialLimits from '../common/credentialLimits';
import { createUser } from '../Requests/Register';
import './Register.css'
import { usernameRegex, passwordRegex } from '../common/constants';

const Register = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [repeatPassword, setRepeatPassword] = useState('');
    const [passwordShown, setPasswordShown] = useState('false');
    const [repeatPasswordShown, setRepeatPasswordShown] = useState('false');
    const [isPasswordInputVisible, setIsPasswordInputVisible] = useState('false');
    const [isRepeatPasswordInputVisible, setIsRepeatPasswordInputVisible] = useState('false')

    let navigate = useNavigate();

    const togglePassword = (e) => {
        e.preventDefault();
        setPasswordShown(!passwordShown);
        setIsPasswordInputVisible(!isPasswordInputVisible)
    }

    const toggleRepeatPassword = (e) => {
        e.preventDefault();
        setRepeatPasswordShown(!repeatPasswordShown);
        setIsRepeatPasswordInputVisible(!isRepeatPasswordInputVisible)
    }

    const triggerRegister = (event) => {
        event.preventDefault();
        createUser(username, password).then(() => navigate('/'));
    };

    const canRegister = () => {
        const isValidUsername = usernameRegex.test(username);
        const isValidPassword = passwordRegex.test(password);

        return isValidUsername
            && isValidPassword
            && repeatPassword === password;
    }

    return (
        <div className="register-form">
            <div className="left-reg">
                <form className="form">
                    <div className="logo"></div>
                    <p className="welcome">WELCOME TO THE BEST BOOK DATABASE</p>
                    <p className="create">CREATE YOUR PROFILE</p>
                    <p className="cred-label">Username</p>
                    <input type="username" onChange={e => setUsername(e.target.value)} value={username} className='cred'></input>
                    <p className="cred-label">Password</p>
                    <input type={passwordShown ? 'password' : 'text'} onChange={e => setPassword(e.target.value)} value={password} className='cred'></input>
                    <button className={"view-btn-register" + (isPasswordInputVisible ? '' : ' passwordInputVislible')} onClick={togglePassword}></button>
                    <div>
                        <p className="cred-label">Repeat password</p>
                        <input type={repeatPasswordShown ? 'password' : 'text'} onChange={e => setRepeatPassword(e.target.value)} value={repeatPassword} className='cred'></input>
                        <button className={"view-btn-register" + (isRepeatPasswordInputVisible ? '' : ' passwordInputVislible')} onClick={toggleRepeatPassword} />
                    </div>
                    <button disabled={!canRegister()} className="signup-button" variant="outline-light" type="submit" onClick={triggerRegister}>
                        SIGN UP
                    </button>
                    <div className="login-label">
                        <span className="existing-account">You Have an account? </span>
                        <Link to='/'>
                            <span className="login-here">LOG IN HERE</span>
                        </Link>
                    </div>

                </form>
            </div>
            <div className="register-background"></div>
        </div>
    )
};

export default Register;