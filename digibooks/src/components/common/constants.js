export const BASE_URL = 'https://books-library-dev.herokuapp.com/api'

export const usernameRegex = /^[a-zA-Z0-9_]{4,}$/;
export const passwordRegex = /^[a-zA-Z0-9]{6,}$/;