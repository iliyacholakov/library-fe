const credentialsLength = {
    passwordMinLength: 6,
    passwordMaxLength: 15,
    usernameMaxLength: 40,
    usernameMinLength: 4,
}

export default credentialsLength;