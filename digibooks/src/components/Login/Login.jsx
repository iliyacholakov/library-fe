import React, { useState } from 'react'
import './Login.css'
import { userLogin } from '../Requests/Login';
import { useNavigate, Link } from 'react-router-dom';
import { usernameRegex, passwordRegex } from '../common/constants';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordShown, setPasswordShown] = useState('false');
  const [isPasswordInputVisible, setIsPasswordInputVisible] = useState ('false');
  let navigate = useNavigate();

  const submitHandler = () => {
    userLogin(username, password).then(() => {
        navigate('/books');
    })
  }

  const togglePassword = (e) => {
    e.preventDefault()
    setPasswordShown(!passwordShown);
    setIsPasswordInputVisible(!isPasswordInputVisible);
  }
      
  const canLogIn = () => {
    const isValidUsername = usernameRegex.test(username);
    const isValidPassword = passwordRegex.test(password);
    
    return isValidUsername
       && isValidPassword;
  }



  return (
    <div className="login-page">
      <form className='login-page-wrapper' onSubmit={e => { e.preventDefault(); submitHandler() }}>
        <div className="login-logo"></div>
        <p className='welcome-sign'>WELCOME BACK</p>
        <p className='label'>Username</p>
        <input type="username" onChange={e => setUsername(e.target.value)} value={username} className="credential-inputs"></input>
        <p className='label'>Password</p>
        <input type={passwordShown ? 'password' : 'text'} onChange={e => setPassword(e.target.value)} value={password} className="credential-inputs"></input>
        <button className={"view-btn" + (isPasswordInputVisible ? '' : ' passwordInputVislible')} onClick={togglePassword}></button>
        <span className='recover-password'>Recover Password</span>
        <button disabled={!canLogIn()} type='submit' className='login-button'>Login</button>
        <div className="login-label">
          <span className="existing-account-login">You Don't Have an account? </span> 
          <Link to='/register'>
            <span className="signup-here">SIGN UP HERE</span>
          </Link>
        </div>
      </form>
      <div className='login-background'></div>
    </div>
  );
};

export default Login;