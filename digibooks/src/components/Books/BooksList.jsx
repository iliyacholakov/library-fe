import './Books.css'
import { Link } from 'react-router-dom';

const formatDates = (dateAsString) => {
  const indexOfDelimiter = dateAsString.indexOf('T');
  const newDate = dateAsString.slice(0,indexOfDelimiter)

  return newDate;
}

const booksList = ({ books }) =>
  books.map((book) => (
    <div key={book._id}>

      <div className="book-card">
        <img className="book-cover" src={book.image} alt={book.name} />

        <div className='book-card-description'>
          <p className="book-name">{book.name}</p>
          <p className="book-author">{book.author}</p>
          <p className="book-genre">Genre: {book.genre.name}</p>
          <span className="book-created">Created on: {formatDates(book.createOn)}</span>
          <span className="book-updated">Updated on: {formatDates(book.lastUpdateOn)}</span>
        </div>

        <div>
          <Link to={`/books/${book._id}`}>
              <div className='specific-book'>
                <p className='polygon' src='' alt=''/>  
              </div>   
          </Link>
        </div>
      </div>

    </div>
  ));

export default booksList;