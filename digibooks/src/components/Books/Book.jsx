import React, { useEffect, useState } from 'react'
import { getSingleBook } from '../Requests/Books';
import { Link, useParams } from 'react-router-dom';
import './Books.css'

function Book() {
  const { id } = useParams();
  const [book, setBook] = useState([]);

  useEffect(() => {
    getSingleBook(id).then((data) => {
    setBook(data);
  })
  }, [id]);

  return (
    <div >
      <div className='book-nav-bar'>
        <div className='back-btn'>
          <Link to='/books'>
            <button className='to-books'></button>
          </Link>
          <span className='library'>Library</span>
        </div>

        <div className='book-library-settings'>
          <Link to='/books'>
            <button>Library</button>
          </Link>
          <Link to='/settings'>
            <button>Settings</button>
          </Link>
        </div>

        <div>
          <div className='profile'></div>
        </div>
      </div>


      <div className='book-wrapper'>
        <div className='book-image'>
          <img src={book.image} alt={book.name} />
        </div>

        <div className='book-description'>
          <div className="name">
            <p>{book.name}</p>
          </div>
          <p className="author">{book.author}</p>
          <p className="created">Created on: {book.createOn}  </p>
          <p className="updated">Updated on: {book.lastUpdateOn}  </p>
          <p className='description-label'>Short Description</p>
          <p className='description'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Blanditiis libero quasi perspiciatis explicabo consequuntur corporis dolore rem dicta amet cum harum sint autem, quaerat odit veritatis ex vel aperiam accusantium. Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate rem quibusdam sit, ipsum deserunt voluptatem tenetur provident vel error beatae ad debitis saepe voluptatibus minima! Vero fuga voluptatum laboriosam nemo!</p>
        </div>
      </div>



    </div>
  )
}

export default Book