import React,{useEffect, useState} from 'react'
import {getBooks, getBooksSearch} from '../Requests/Books'
import BooksList from './BooksList'
import './Books.css'
import { Link } from 'react-router-dom'

const Books = () => {
  
  const [books, setBooks] = useState([]);
  const [phrase, setPhrase] = useState('');

  useEffect(()=>{
    getBooks().then((data) => setBooks(data));
  },[])

  const searchHandler = (phrase) =>{
    if (phrase === '') {
      getBooks().then((data) => setBooks(data));
      return;
    }
    getBooksSearch(phrase).then((data) => setBooks(data));
  }

  return (
    <div>
        <div className="nav-bar">
          <div>
            <p className='nav-bar-logo'></p>
          </div>
          <div className='settings-library'>
            <Link to='/books'>
              <button className="library">Library</button>
            </Link>
            <Link to='/settings'>
              <button className="settings">Settings</button>
            </Link>
          </div>
          <div>
            <div className='profile'></div>
          </div>
        </div>
        <div className="container">
          <div className='search'>
            <p className='search-label'>All Books</p>
            <input type='text' placeholder='Search'onChange={e => setPhrase(e.target.value)} value={phrase} className="search-input"></input>
            <button onClick={() => searchHandler(phrase)} className='search-button'></button>
          </div>

          <div className='books-wrapper'>
            <BooksList books={books}/>
          </div>
        </div>
    </div>

  );
};

export default Books;