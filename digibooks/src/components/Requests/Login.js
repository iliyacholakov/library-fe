import { BASE_URL } from "../common/constants"

export const userLogin = (username, password) => {
  return fetch(`${BASE_URL}/user/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username,
      password,

    })
  })
    .then(res => res.json())
    .then(res => localStorage.setItem("token", res.token));
};