import { BASE_URL } from "../common/constants"

export const userLogout = () => {
  return fetch(`${BASE_URL}/user/logout`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
       Authorization: `Bearer ${localStorage.getItem("token")}`
    }
  })
    .then(() => localStorage.removeItem("token"));
};