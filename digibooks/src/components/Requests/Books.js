import { BASE_URL } from "../common/constants"

export const getBooks = () => {
    return fetch(`${BASE_URL}/book`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
           Authorization: `Bearer ${localStorage.getItem("token")}` 
        },
      })
        .then(res => res.json());
}
export const getSingleBook = (id) => {
    return fetch(`${BASE_URL}/book/${id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
           Authorization: `Bearer ${localStorage.getItem("token")}`
        },
      })
        .then(res => res.json());
}

export const getBooksSearch = (phrase) => {
    return fetch(`${BASE_URL}/book/search`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
         Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body:JSON.stringify({
        pattern: `${phrase}`,
      })
    })
      .then(res => res.json());
  }
