import { BASE_URL } from "../common/constants"

export const createUser = (username, password) => {
  return fetch(`${BASE_URL}/user/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username,
      password,

    })
  })
    .then(res => res.json());
};