import React from 'react'
import './Settings.css'
import { Link } from 'react-router-dom'

function Settings() {
  return (
<div>
    <div className="nav-bar">
          <div>
            <p className='nav-bar-logo'></p>
          </div>
          <div className='settings-library'>
            <Link to='/books'>
              <button className="library">Library</button>
            </Link>
            <Link to='/settings'>
              <button className="settings">Settings</button>
            </Link>
          </div>
          <div>
            <div className='profile'></div>
          </div>
        </div>
    <div className='settings-wrapper'>

      <div className='left-side'>
        <p className='general-book'>GENERAL SETTINGS</p>
        <div className='paragraph'>
          <p className='paragraph-text'>NOTIFICATIONS AND EMAILS</p>
          <hr></hr>
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>USER MANAGEMENT</p>
          <hr></hr>
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>PHYSICAL LIBRARIES</p>
          <hr></hr>
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>READING EVENTS</p>
          <hr></hr>
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>INVOICING</p>
          <hr></hr>
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>BOOK STATISTICS</p>
          <hr></hr>
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>READERS STATISTICS</p>
          <hr></hr>
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>EVENTS STATISTICS</p>
          <hr></hr>
          <i className='blue-arrow'></i>
        </div>
      </div>

      <div className='right-side'>
        <div className='wrap'>
          <p className='general-book'>BOOK SETTINGS</p>
          <button className='add-new'>ADD NEW</button>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>MANAGE GENRES</p>
          <hr></hr> 
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>BOOK VISIBILITY</p>
          <hr></hr> 
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>AUTHORS DATABASE</p>
          <hr></hr> 
          <i className='blue-arrow'></i>
        </div>
        <div className='paragraph'>
          <p className='paragraph-text'>BOOK COVERS</p>
          <hr></hr> 
          <i className='blue-arrow'></i>
        </div>
      </div>
    </div>
</div>
  )
}

export default Settings